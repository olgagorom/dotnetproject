﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for RoomStatusWindow.xaml
    /// </summary>
    public partial class RoomStatusWindow : MetroWindow
    {
        List<RoomWithReserv> RoomListOccupied = new List<RoomWithReserv>();
        List<RoomWithReserv> RoomListReserved = new List<RoomWithReserv>();

        List<RoomWithReserv> RoomListAvailable = new List<RoomWithReserv>();
        DateTime plusOneYearDate = DateTime.Now.AddYears(1);
        DateTime currentDate = DateTime.Now;



        public RoomStatusWindow(Window owner)
        {
            InitializeComponent();
            Owner = owner;

            //RoomList = Globals.Db.GetAllRoomsWithReserv();
            LoadRoomsList();
            lvRoomOccupied.ItemsSource = RoomListOccupied;

            lvRoomReserved.ItemsSource = RoomListReserved;

            lvRoomAvailable.ItemsSource = RoomListAvailable;



        }

        // LOAD ALL ROOMS STATUS
        public void LoadRoomsList()
        {
            try
            {
                //ROOM OCCUPIED
                List<RoomWithReserv> listO = Globals.Db.GetAllRoomsWithReserv();
                var occupied = from item in listO where item.CheckIn < currentDate && currentDate < item.CheckOut select item;
                listO = occupied.ToList();
                // Clear old ItemsList
                RoomListOccupied.Clear();
                // Add new list to ItemsList
                RoomListOccupied.AddRange(listO.ToList());
                lvRoomOccupied.Items.Refresh();

                //ROOM RESERVED
                List<RoomWithReserv> listR = Globals.Db.GetAllRoomsWithReserv();
                var reserv = from row in listR where row.CheckIn > currentDate && plusOneYearDate > row.CheckOut select row;
                listR = reserv.ToList();
                RoomListReserved.Clear();
                RoomListReserved.AddRange(listR.ToList());
                lvRoomReserved.Items.Refresh();

                //ROOM HAD RESERVE
                List<RoomWithReserv> listU = Globals.Db.GetAllRoomsWithReserv();
                var reservOut = from row in listU where row.CheckOut < currentDate select row;
                listU = reservOut.ToList();
                RoomListAvailable.Clear();
                RoomListAvailable.AddRange(listU.ToList());

                //ROOM AVAILABLE
                List<RoomWithReserv> listA = Globals.Db.GetAllRoomsAvailable();
                var avail = from row in listA select row;
                listA = avail.ToList();
                RoomListAvailable.AddRange(listA.ToList());
                lvRoomAvailable.Items.Refresh();

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NewReNewReservContextMenu_Click(object sender, RoutedEventArgs e)
        {
            var win = new AddNewReservationWizard();
            win.ShowDialog();
        }
    }
}
