﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class RoomAvailable
    {
        
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public decimal PricePerNight { get; set; }
        public string MaxPerson { get; set; }
        public ERoomType RoomType { get; set; }

       public enum ERoomType { Single = 2, Double = 1 }
    }
}
