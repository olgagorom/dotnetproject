﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Payment
    {
        public int PaymentId { get; set; }
        public string CreditCardNumber { get; set; }
        public string CVCNumber { get; set; }
        public DateTime ExpiredDate { get; set; }
        public ECardType CardType { get; set; }
    }
    public enum ECardType { Visa, MasterCard };
}
