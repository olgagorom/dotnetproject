﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for RoomAvailableByDate.xaml
    /// </summary>
    public partial class RoomAvailableByDate : MetroWindow
    {
        RoomWithReserv RoomWithReserv;

        List<RoomWithReserv> RoomListAvailablebyDate = new List<RoomWithReserv>();
        List<RoomWithReserv> RoomListAvailable = new List<RoomWithReserv>();


        public RoomAvailableByDate(Window owner)
        {
            InitializeComponent();
            Owner = owner;
           // LoadRoomsListbyDate();
           // lvRoomAvailablebyDate.ItemsSource = RoomListAvailablebyDate;
            lvRoomAvailablebyDate.ItemsSource =Globals.Db.GetAllRoomsAvailableByDate();
        }



        public RoomAvailableByDate(Window owner, RoomWithReserv roomWithReserv = null)
        {
            InitializeComponent();
            Owner = owner;

            RoomWithReserv = roomWithReserv;
            if (RoomWithReserv != null)
            {
                dpDateIn.SelectedDate = RoomWithReserv.CheckIn;
                dpDateOut.SelectedDate = RoomWithReserv.CheckOut;
                
            }
            LoadRoomsListbyDate();
            lvRoomAvailablebyDate.ItemsSource = RoomListAvailablebyDate;
            lvRoomAvailablebyDate.ItemsSource = RoomListAvailable;
        }

        public void LoadRoomsListbyDate()
        {
            DateTime currentDate = DateTime.Now;
            DateTime dateIn = (DateTime)dpDateIn.SelectedDate;
            DateTime dateOut = (DateTime)dpDateOut.SelectedDate;

           //EnRoomType typeRoom = EnRoomType.Single;
            //EnRoomType typeRoom = (EnRoomType)cmbRoomType.SelectedValue;

           
           // TaskStatus isDone = (cmbIsDone.IsChecked == true ? TaskStatus.Done : TaskStatus.Pending);
            try
            {
                List<RoomWithReserv> list = Globals.Db.GetAllRoomsWithReserv();
                var reserv = from item in list where !(dateIn < item.CheckIn) && !(item.CheckIn < dateOut) select item;

                list = reserv.ToList();
                // Clear old ItemsList
                RoomListAvailablebyDate.Clear();
                // Add new list to ItemsList
                RoomListAvailablebyDate.AddRange(list);
                lvRoomAvailablebyDate.Items.Refresh();

                // ROOM HAD RESERVE
                List<RoomWithReserv> listU = Globals.Db.GetAllRoomsWithReserv();
                var reservOut = from row in listU where row.CheckOut < currentDate select row;
                listU = reservOut.ToList();
                RoomListAvailable.Clear();
                RoomListAvailable.AddRange(listU.ToList());

                List<RoomWithReserv> listA = Globals.Db.GetAllRoomsAvailable();
                var avail = from row in listA select row;
                listA = avail.ToList();
                RoomListAvailable.AddRange(listA.ToList());
                lvRoomAvailablebyDate.Items.Refresh();

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NewReservContextMenu_Click(object sender, RoutedEventArgs e)
        {
            var win = new AddNewReservationWizard();
            win.ShowDialog();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            LoadRoomsListbyDate();
        }
    }
}
