﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HotelManagementSystem
{

    /// <summary>
    /// Interaction logic for AddNewReservationWizard.xaml
    /// </summary>
    public partial class AddNewReservationWizard : MetroWindow
    {
 
        string firstName;
        string lastName;
        string address;
        string postalCode;
        string country;
        string city;
        string email;
        string phone;
        string comment;
        string cvcnumber;
        EBreakfast breakfast;
        ECardType cardType;
        int fkRoomId;
        int fKPaymentId;
        decimal fkRoomPrice;
        int nights;
        decimal currBill;
        DateTime regTime;
        string creditCardNumber;
        decimal breakfastPrice = 7;
       
        public AddNewReservationWizard()
        {
            InitializeComponent();
            dpCheckIn.SelectedDate = DateTime.Today;
            dpExpiredDate.SelectedDate = DateTime.Today;
            try { 
            lvRoomAvailable.ItemsSource = Globals.Db.GetAllRoomsAvailableByDate();
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    MessageBox.Show("Error : failed to save reservation into database:\n" + ex.Message, "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    throw ex;
                }
            }
        }

        private void ClickNext(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            Xceed.Wpf.Toolkit.Wizard Wizard = sender as Xceed.Wpf.Toolkit.Wizard;

            if (Wizard.CurrentPage.Name == "Page1")
            {

                if (lvRoomAvailable.SelectedItem == null)
                {
                    MessageBox.Show("Choose room avalable.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (dpCheckIn.SelectedDate == null)
                {
                    MessageBox.Show("Choose checkin date.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (dpCheckOut.SelectedDate == null)
                {
                    MessageBox.Show("Choose checkout date.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (dpCheckIn.SelectedDate > dpCheckOut.SelectedDate)
                {
                    MessageBox.Show("CheckOut date must be greater than Check In Date.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }

                fkRoomId = (lvRoomAvailable.SelectedValue as RoomWithReserv).RoomId;
                fkRoomPrice = (lvRoomAvailable.SelectedValue as RoomWithReserv).PricePerNight;

            }
            else if (Wizard.CurrentPage.Name == "Page2")
            {
                DateTime checkIn = dpCheckIn.SelectedDate.Value;
                DateTime checkOut = dpCheckOut.SelectedDate.Value;

                firstName = tbFirstName.Text;
                lastName = tbLastName.Text;
                address = tbAddress.Text;
                postalCode = tbPostalCode.Text;
                country = tbCountry.Text;
                city = tbCity.Text;
                email = tbEmail.Text;
                phone = tbPhone.Text;
                comment = tbComment.Text;
                breakfast = (chbBreakfast.IsChecked == true ? EBreakfast.Include : EBreakfast.NotInclude);
                regTime = DateTime.Today;
                //validation firstName
                if (firstName.Length < 1 || firstName.Length > 20)
                {
                    MessageBox.Show("Guest First Name must be between 1 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(firstName, @"^[a-zA-Z]+$"))
                {
                    MessageBox.Show("The username must only contain letters (a-z, A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation LastName
                if (lastName.Length < 1 || lastName.Length > 20)
                {
                    MessageBox.Show("Guest Last Name must be between 1 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(lastName, @"^[a-zA-Z]+$"))
                {
                    MessageBox.Show("User last name must only contain letters (a-z, A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation min age for a guest

                if (dpBirthday.SelectedDate == null)
                {
                    MessageBox.Show("Choose guest birthdayDate\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (DateTime.Now.Year - dpBirthday.SelectedDate.Value.Year < 20)
                {
                    MessageBox.Show("Age must be more then 20\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation Address
                if (address.Length > 60)
                {
                    MessageBox.Show("Guest address must be 60 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation PostalCode
                if (postalCode.Length > 10)
                {
                    MessageBox.Show("Guest postal code must be between 0 and 10 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation Country
                if (country.Length > 20)
                {
                    MessageBox.Show("Guest country must be between 0 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(country, @"^*$|^[a-zA-Z]+$"))
                {
                    MessageBox.Show("Guest country must contain only letters (a-z, A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation City
                if (city.Length > 20)
                {
                    MessageBox.Show("Guest city must be between 0 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(city, @"^\s*$|^[a-zA-Z]+$"))
                {
                    MessageBox.Show("Guest city must only contain letters (a-z,A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation Email
                if (email.Length > 60)
                {
                    MessageBox.Show("Guest email must be between 8 and 60 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(email, @"^\s*$|^^[a-z0-9][-a-z0-9._]+@([-a-z0-9]+\.)+[a-z]{2,5}$"))
                {
                    MessageBox.Show("Guest email is incorrect\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                //validation phone number
                if (phone.Length > 24)
                {
                    MessageBox.Show("Guest phone must be between 0 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(phone, @"^\s*$|^[0-9]+$"))
                {
                    MessageBox.Show("Guest phone must only contain numbers (0-9)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (chbBreakfast == null)
                {
                    MessageBox.Show("Check Box Breakfast does not exists.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (chbBreakfast.IsChecked == true)
                {
                    TbBreakfastBill.Text = String.Format("$ {0:0.##}", breakfastPrice);
                }
                //Fixme add handler // if page does not exists
                //else
                //{
                //    MessageBox.Show("Intern error: Page not found", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                //    //e.Cancel = true;
                //    return;
                //}

                nights = (checkOut - checkIn).Days;
                currBill = fkRoomPrice * nights;
                tbCurrentBill.Text = String.Format("$ {0:0.##}", currBill);
                decimal total = currBill + breakfastPrice;
                decimal tax = total / 100 * 15m;
                tbTax.Text = String.Format("$ {0:0.##}", tax);
                tbTotal.Text = String.Format("$ {0:0.##}", total + tax);
            }

        }

        private void ClickFinish(object sender, Xceed.Wpf.Toolkit.Core.CancelRoutedEventArgs e)
        {
            DateTime birthday = dpBirthday.SelectedDate.Value;
            DateTime expiredCardDate = dpExpiredDate.SelectedDate.Value;
            DateTime checkIn = dpCheckIn.SelectedDate.Value;
            DateTime checkOut = dpCheckOut.SelectedDate.Value;
            creditCardNumber = tbCreditCardNumber.Text;
            cvcnumber = tbCVC.Text;

                //Data verification
                if (dpExpiredDate.SelectedDate.Value == null)
                {
                    MessageBox.Show("Choose expired date for credit card\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (rbVisa.IsChecked == true)
                {
                    cardType = ECardType.Visa;
                }
                else if (rbMasterCard.IsChecked == true)
                {
                    cardType = ECardType.MasterCard;
                }
                else
                {
                    MessageBox.Show("Internal error, invalid Card Type", "Card Type", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (tbCurrentBill == null)
                {
                    MessageBox.Show("Current bill must be field out.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (creditCardNumber.Length != 12)
                {
                    MessageBox.Show("Credit Card number must be 12 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(creditCardNumber, @"^[0-9]{12}$"))
                {
                    MessageBox.Show("Credit Card number must only contain numbers (0-9)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (cvcnumber.Length != 3)
                {
                    MessageBox.Show("CVCnumber must be 3 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (!Regex.IsMatch(cvcnumber, @"^[0-9]{3}$"))
                {
                    MessageBox.Show("CVCnumber number must only contain numbers (0-9)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Cancel = true;
                    return;
                }
                if (dpExpiredDate.SelectedDate == null)
                    {
                        MessageBox.Show("Choose expired date for credit card\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                        e.Cancel = true;
                        return;
                    }
                
            try
            {
                Payment Payment = new Payment() { CreditCardNumber = creditCardNumber, CVCNumber = cvcnumber, CardType = cardType, ExpiredDate = expiredCardDate};
                 fKPaymentId = Globals.Db.AddPayment(Payment);

                Reservation Reservation = new Reservation() { FirstName = firstName, LastName = lastName, Address = address, PostalCode = postalCode, Country = country, City = city, Phone = phone, CheckIn = checkIn, CheckOut = checkOut, RegTime=regTime, Birthday = birthday, Breakfast = breakfast, Comment = comment, Email = email,FKRoomId = fkRoomId, FKPaymentId = fKPaymentId };

                Globals.Db.AddReservation(Reservation);
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    MessageBox.Show("Error : failed to save reservation into database:\n" + ex.Message, "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    throw ex;
                }
            }
        }

    }
}
