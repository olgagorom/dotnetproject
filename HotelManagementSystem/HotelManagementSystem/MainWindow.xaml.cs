﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {

                    MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "HotelManagement System", MessageBoxButton.OK, MessageBoxImage.Error);
                    Close(); // close the main window, terminate the program
                }
                else
                {
                    throw ex;
                }
            }
        }




        private void ModifyReservation_Click(object sender, RoutedEventArgs e)
        {
            ModifyDialog ModifyDialog = new ModifyDialog(this);
            if (ModifyDialog.ShowDialog() == true)
            {
                ModifyDialog.ReloadList();
            }
        }


        // ROOM STATUS
        private void TileRoomStatus_Click(object sender, RoutedEventArgs e)
        {
            RoomStatusWindow RoomStatus = new RoomStatusWindow(this);
           if (RoomStatus.ShowDialog() == true)
            {
                RoomStatus.LoadRoomsList();
            }
        }

        private void AddNewReservation_Click(object sender, RoutedEventArgs e)
        {
            /* AddUpdateDialog AddUpdate = new AddUpdateDialog(this);
             if (AddUpdate.ShowDialog() == true)
             {
                 //implement code
             }*/

            var win = new AddNewReservationWizard();
            win.ShowDialog();
            //MessageBox.Show("Fihished succsesfully!"); fixme show when we finished
        }



        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime currentDate = DateTime.Now;
            if (dpDateIn.SelectedDate < currentDate || dpDateIn.SelectedDate > dpDateOut.SelectedDate || dpDateIn.SelectedDate == null || dpDateOut.SelectedDate == null)
            {
                MessageBox.Show("You must input date", "Room status", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            DateTime dateIn = (DateTime)dpDateIn.SelectedDate;
            DateTime dateOut = (DateTime)dpDateOut.SelectedDate;
            RoomWithReserv roomWithReserv = new RoomWithReserv() { CheckIn = dateIn, CheckOut = dateOut };



            RoomAvailableByDate RoomStatusByDate = new RoomAvailableByDate(this, roomWithReserv);
            if (RoomStatusByDate.ShowDialog() == true)
            {
                RoomStatusByDate.LoadRoomsListbyDate();
            }
        }
    }
}
