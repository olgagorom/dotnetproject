﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Users
    {
        public int UserID { get; set; }
        public ETaskStatus UserType { get; set; }
        public int UserName { get; set; }
        public int UserPassword { get; set; }
    }
    public enum ETaskStatus { Admin, DeskManager, Kitchen };
}
