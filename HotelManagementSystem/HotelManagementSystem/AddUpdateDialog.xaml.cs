﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;


namespace HotelManagementSystem
{ 


  /*--------------------------------------------*/
  // This window was originally designed for add and modify reservations
  // This code need to be redone for modifications in Reservations
  // because wizard was added to solve "add new reservation" part
  //
  //
  /*--------------------------------------------*/





    /// <summary>
    /// Interaction logic for AddUpdateDialog.xaml
    /// </summary>
    public partial class AddUpdateDialog : MetroWindow
    {
        Reservation EditedReserv;
        List<RoomWithReserv> roomList = Globals.Db.GetAllRoomsAvailable();
        
        public AddUpdateDialog(Window owner, Reservation editedReserv = null)
        {
            InitializeComponent();
            Owner = owner;
            dpCheckIn.SelectedDate = DateTime.Today;
            EditedReserv = editedReserv;
            if (EditedReserv != null)
            {
                btSubmitAdd.Content = "Update";
                //TODO: Prepare data 
                //tbFirstName.Text = EditedReserv.FirstName;
            }
        }

        //TODO Add combobox item == rooms available 
        //begin combobox code
        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            //query to select rom available
            var roomAvail = from room in roomList select room.RoomNumber;

            // ... Get the ComboBox reference.
            var comboBox = sender as ComboBox;

            // ... Assign the ItemsSource to the List.
            comboBox.ItemsSource = roomAvail;

            // ... Make the first item selected.
            comboBox.SelectedIndex = 0;

        }
        
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
         {
             // ... Get the ComboBox.
             var comboBox = sender as ComboBox;

            //
            //ERoomType roomType = from room in roomList where room.RoomNumber == comboBox.SelectedItem select room.RoomType;
           // lblRoomType.Content = roomType;

            // ... Set SelectedItem as label content.
            //string value = comboBox.SelectedItem as string;
            //lblRoomType.Content = value;
         }
        //end add combobox code

         //Button click handler ro save or modify reservations
        private void ButtonAddUpdate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string firstName = tbFirstName.Text;
                //validation firstName
                if (firstName.Length < 4 || firstName.Length > 20)
                {
                    MessageBox.Show("Guest First Name must be between 4 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Regex.IsMatch(firstName, @"^[a-zA-Z]+$"))
                {
                    MessageBox.Show("The username must only contain letters (a-z, A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                string lastName = tbLastName.Text;
                //validation LastName
                if (lastName.Length < 4 || lastName.Length > 20)
                {
                    MessageBox.Show("Guest Last Name must be between 4 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Regex.IsMatch(lastName, @"^[a-zA-Z]+$"))
                {
                    MessageBox.Show("User last name must only contain letters (a-z, A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                //validation Address
                string address = tbAddress.Text;
                if (address.Length < 4 || address.Length > 60)
                {
                    MessageBox.Show("Guest address must be between 4 and 60 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                //validation PostalCode
                string postalCode = tbPostalCode.Text;
                if (postalCode.Length > 10)
                {
                    MessageBox.Show("Guest postal code must be between 0 and 10 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                //validation Country
                string country = tbCountry.Text;
                if (country.Length > 20)
                {
                    MessageBox.Show("Guest country must be between 0 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Regex.IsMatch(country, @"^[a-zA-Z]+$"))
                {
                    MessageBox.Show("Guest country must only contain letters (a-z, A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                string city = tbCity.Text;
                //validation City
                if (city.Length > 20)
                {
                    MessageBox.Show("Guest city must be between 0 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Regex.IsMatch(city, @"^[a-zA-Z]+$"))
                {
                    MessageBox.Show("Guest city must only contain letters (a-z,A-Z)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                string email = tbEmail.Text;
                if (email.Length > 60)
                {
                    MessageBox.Show("Guest email must be between 8 and 60 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Regex.IsMatch(email, @"^^[a-z0-9][-a-z0-9._]+@([-a-z0-9]+\.)+[a-z]{2,5}$"))
                {
                    MessageBox.Show("Guest email is incorrect\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                string phone = tbPhone.Text;
                if (phone.Length > 24)
                {
                    MessageBox.Show("Guest phone must be between 0 and 20 characters long.\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!Regex.IsMatch(city, @"^[0-9]+$"))
                {
                    MessageBox.Show("Guest phone must only contain numbers (0-9)\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                //DateTime regTime = DateTime.Now;
                DateTime checkIn = dpCheckIn.SelectedDate.Value;
                DateTime checkOut = dpCheckOut.SelectedDate.Value;
                EBreakfast breakfast = (chbBreakfast.IsChecked == true ? EBreakfast.Include : EBreakfast.NotInclude);
                int nights = (checkOut - checkIn).Days;
                string comment = tbComment.Text;
                DateTime birthday = dpBirthday.SelectedDate.Value;
                //validation min age for a guest
                if ((DateTime.Today - birthday).TotalDays < (20 * 365))
                {
                    MessageBox.Show("Age must be more then 20\n", "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                

                //foreign key
                string fkPaymentIdStr = tbPaymentForTest.Text;
                int fkPaymentId;
                if (!int.TryParse(fkPaymentIdStr, out fkPaymentId))
                {
                    MessageBox.Show("Invalid input: value must be an integer");
                    return;
                }

                //string fkRoomIdStr = tbRoomNumber.Text;
                // int fkRoomId;
                // if (!int.TryParse(fkRoomIdStr, out fkRoomId))
                // {
                //     MessageBox.Show("Invalid input: value must be an integer");
                //     return;
                // }

                if (EditedReserv == null)
                {
                    Reservation Reservation = new Reservation() { FirstName = firstName, LastName = lastName, Address = address, PostalCode = postalCode, Country = country, City = city, Phone = phone, CheckIn = checkIn, CheckOut = checkOut, Birthday = birthday, Breakfast = breakfast, Comment = comment, Email = email,/* FKRoomId = fkRoomId, */ FKPaymentId = fkPaymentId };

                    Globals.Db.AddReservation(Reservation);
                }
                else
                {
                    // TODO: Database Update
                    // Globals.Db.UpdateItem(EditedItem);
                }
                DialogResult = true;
            }
            catch (Exception ex)
            {
                if (ex is SqlException)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    throw ex;
                }
            }
        }

        private void ButtonPayment_Click(object sender, RoutedEventArgs e)
        {
            //PaymentDialog PaymentDialog = new PaymentDialog(this);
            //if (PaymentDialog.ShowDialog() == true)
            //{
            //    //implement code
            //}
        }
    }
        
}
