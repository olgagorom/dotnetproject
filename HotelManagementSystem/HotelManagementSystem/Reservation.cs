﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Reservation
    {
        public int ReservId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime RegTime { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public EBreakfast Breakfast { get; set; }
        public string Comment { get; set; }
        public DateTime Birthday { get; set; }
        
        //foreign key
        public int FKPaymentId { get; set; }
        public int FKRoomId { get; set; }

    }

    public enum EBreakfast { Include, NotInclude };
}
