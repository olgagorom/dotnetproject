﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class Database
    {
        // Set DbConnectionString from DB Property

        const string DbConnectionString = @"Server=tcp:ipd16-dotnet.database.windows.net,1433;Initial Catalog=DotNetProject;Persist Security Info=False;User ID=Dmitriy;Password=Admin555;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        private SqlConnection conn;

        // DB Constructor
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        //This method retrives list of all rooms return list of the rooms
        public List<Room> GetAllRooms()
        {
            List<Room> list = new List<Room>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Rooms", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int roomId = (int)reader["RoomId"];
                    string roomNumber = (string)reader["RoomNumber"];
                    ERoomType roomType = (ERoomType)Enum.Parse(typeof(ERoomType), (string)reader["RoomType"]);
                    decimal pricePerNight = (decimal)reader["PricePerNight"];
                    string maxPerson = (string)reader["MaxPerson"];
                    ERoomStatus roomStatus = (ERoomStatus)Enum.Parse(typeof(ERoomStatus), (string)reader[5]);
                    list.Add(new Room() { RoomId = roomId, RoomNumber = roomNumber, RoomType = roomType, PricePerNight = pricePerNight, MaxPerson = maxPerson, RoomStatus = roomStatus });
                }
            }
            return list;
        }

        //This method retrives list of all rooms  with reservation and return list of the rooms
        public List<RoomWithReserv> GetAllRoomsWithReserv()
        {

            List<RoomWithReserv> list = new List<RoomWithReserv>();
            SqlCommand cmdSelect = new SqlCommand("SELECT ro.RoomNumber as [RoomNumber], rs.CheckIn as [CheckIn], rs.CheckOut as [CheckOut], [RoomType], [FirstName], [LastName], rs.Phone as [Phone], ro.RoomId as [RoomId], ro.PricePerNight as [PricePerNight], ro.MaxPerson as [MaxPerson] FROM Reservations as rs INNER JOIN Rooms as ro ON rs.RoomId = ro.RoomId", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int roomId = (int)reader["RoomId"];
                    string roomNumber = (string)reader["RoomNumber"];
                    EnRoomType roomType = (EnRoomType)Enum.Parse(typeof(EnRoomType), (string)reader["RoomType"]);
                    decimal pricePerNight = (decimal)reader["PricePerNight"];
                    string maxPerson = (string)reader["MaxPerson"];
                    DateTime checkIn = (DateTime)reader["CheckIn"];
                    DateTime checkOut = (DateTime)reader["CheckOut"];
                    string firstName = (string)reader["FirstName"];
                    string lastName = (string)reader["LastName"];
                    string phone = (string)reader["Phone"];

                    //ERoomStatus roomStatus = (ERoomStatus)Enum.Parse(typeof(ERoomStatus), (string)reader[5]);
                    list.Add(new RoomWithReserv() { RoomId = roomId, RoomNumber = roomNumber, RoomType = roomType, PricePerNight = pricePerNight, MaxPerson = maxPerson, CheckIn = checkIn, CheckOut = checkOut, FirstName = firstName, LastName = lastName, Phone = phone });
                }
            }
            return list;
        }


        //This method retrives list of all rooms available and return list of the rooms
        public List<RoomWithReserv> GetAllRoomsAvailable()
        {
            List<RoomWithReserv> list = new List<RoomWithReserv>();
            SqlCommand cmdSelect = new SqlCommand("SELECT ro.RoomNumber as [RoomNumber], [RoomType],  ro.PricePerNight as [PricePerNight], ro.MaxPerson as [MaxPerson] FROM Rooms as ro LEFT OUTER JOIN Reservations as rs ON ro.RoomId = rs.RoomId where  rs.CheckOut is null", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    string roomNumber = (string)reader["RoomNumber"];
                    ERoomType roomType = (ERoomType)Enum.Parse(typeof(ERoomType), (string)reader["RoomType"]);
                    decimal pricePerNight = (decimal)reader["PricePerNight"];
                    string maxPerson = (string)reader["MaxPerson"];


                    list.Add(new RoomWithReserv() { RoomNumber = roomNumber, PricePerNight = pricePerNight, MaxPerson = maxPerson/*, RoomType = roomType*/ });
                }
            }
            return list;
        }


        //This method retrives list of all reservations avalable check by date and return list of the reservations
        public List<RoomWithReserv> GetAllRoomsAvailableByDate()
        {

            List<RoomWithReserv> list = new List<RoomWithReserv>();
            SqlCommand cmdSelect = new SqlCommand("SELECT ro.RoomId as [RoomId], ro.RoomNumber as [RoomNumber], rs.CheckIn as [CheckIn], rs.CheckOut as [CheckOut], [RoomType], ro.PricePerNight as [PricePerNight], ro.MaxPerson as [MaxPerson] FROM Rooms as ro LEFT OUTER JOIN Reservations as rs ON ro.RoomId = rs.RoomId", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int roomId = (int)reader["RoomId"];
                    string roomNumber = (string)reader["RoomNumber"];
                    EnRoomType roomType = (EnRoomType)Enum.Parse(typeof(EnRoomType), (string)reader["RoomType"]);
                    decimal pricePerNight = (decimal)reader["PricePerNight"];
                    string maxPerson = (string)reader["MaxPerson"];
                    // DateTime checkIn = (DateTime)reader["CheckIn"];
                    // DateTime checkOut = (DateTime)reader["CheckOut"];

                    //ERoomStatus roomStatus = (ERoomStatus)Enum.Parse(typeof(ERoomStatus), (string)reader[5]);
                    list.Add(new RoomWithReserv() { RoomId = roomId, RoomNumber = roomNumber, RoomType = roomType, PricePerNight = pricePerNight, MaxPerson = maxPerson/*, CheckIn = checkIn, CheckOut = checkOut*/ });
                }
            }
            return list;
        }


        //This method retrives list of all reservations avalable and return list of the reservations
        public List<Reservation> GetAllReservations()
        {
            List<Reservation> list = new List<Reservation>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Reservations", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    //TODO modify code
                    int reservId = (int)reader["ReservID"];
                    string firstName = (string)reader["FirstName"];
                    string lastName = (string)reader["LastName"];
                    string address = (string)reader["Address"];
                    string postalCode = (string)reader["PostalCode"];
                    string country = (string)reader["Country"];
                    string city = (string)reader["City"];
                    string email = (string)reader["Email"];
                    string phone = (string)reader["Phone"];
                    DateTime regTime = (DateTime)reader["RegTime"];
                    DateTime checkIn = (DateTime)reader["CheckIn"];
                    DateTime checkOut = (DateTime)reader["CheckOut"];
                    DateTime birthday = (DateTime)reader["Birthday"];
                    EBreakfast breakfast = (EBreakfast)Enum.Parse(typeof(EBreakfast), (string)reader["Breakfast"]);
                    int fkpaymentid = (int)reader["PaymentId"];
                    int fkroomid = (int)reader["RoomID"];
                    string comment = (string)reader["Comment"];
                    list.Add(new Reservation { ReservId = reservId, FirstName = firstName, LastName = lastName, Address = address, PostalCode = postalCode, Country = country, City = city, Phone = phone, RegTime = regTime, CheckIn = checkIn, CheckOut = checkOut, Birthday = birthday, Breakfast = breakfast, Comment = comment, Email = email, FKRoomId = fkroomid, FKPaymentId = fkpaymentid });
                }
            }
            return list;
        }



        //This method add new reservation and return reservations ID
        public int AddReservation(Reservation item)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Reservations (FirstName, LastName, Address, PostalCode, Country, City, Phone, RegTime, CheckIn, CheckOut, Breakfast, Comment, Email, PaymentId, RoomID, Birthday) OUTPUT INSERTED.ReservID VALUES (@FirstName, @LastName, @Address, @PostalCode, @Country, @City, @Phone, @RegTime, @CheckIn, @CheckOut, @Breakfast, @Comment, @Email, @PaymentId, @RoomID, @Birthday)", conn);
            cmdInsert.Parameters.AddWithValue("FirstName", item.FirstName);
            cmdInsert.Parameters.AddWithValue("LastName", item.LastName);
            cmdInsert.Parameters.AddWithValue("Address", item.Address);
            cmdInsert.Parameters.AddWithValue("PostalCode", item.PostalCode);
            cmdInsert.Parameters.AddWithValue("Country", item.Country);
            cmdInsert.Parameters.AddWithValue("City", item.City);
            cmdInsert.Parameters.AddWithValue("Phone", item.Phone);
            cmdInsert.Parameters.AddWithValue("RegTime", item.RegTime);
            cmdInsert.Parameters.AddWithValue("CheckIn", item.CheckIn);
            cmdInsert.Parameters.AddWithValue("CheckOut", item.CheckOut);
            cmdInsert.Parameters.AddWithValue("Breakfast", item.Breakfast.ToString());// Convert to string
            cmdInsert.Parameters.AddWithValue("Comment", item.Comment);
            cmdInsert.Parameters.AddWithValue("Email", item.Email);
            cmdInsert.Parameters.AddWithValue("PaymentId", item.FKPaymentId);
            cmdInsert.Parameters.AddWithValue("RoomId", item.FKRoomId);
            cmdInsert.Parameters.AddWithValue("Birthday", item.Birthday);

            int insertId = (int)cmdInsert.ExecuteScalar();
            item.ReservId = insertId;
            return insertId;
        }

        //This method delete reservation
        public bool DeleteReservation(int id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Reservations WHERE reservId=@ReservId", conn);
            cmdDelete.Parameters.AddWithValue("ReservId", id);
            int rowsAffected = cmdDelete.ExecuteNonQuery();
            return rowsAffected > 0;
        }

        //This method add new reservation and return reservations ID
        public int AddPayment(Payment item)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Payments (CreditCardNumber, CVCNumber, CardType, ExpiredDate) OUTPUT INSERTED.PaymentId VALUES (@CreditCardNumber, @CVCNumber, @CardType, @ExpiredDate)", conn);
            cmdInsert.Parameters.AddWithValue("CreditCardNumber", item.CreditCardNumber);
            cmdInsert.Parameters.AddWithValue("CVCNumber", item.CVCNumber);
            cmdInsert.Parameters.AddWithValue("CardType", item.CardType.ToString());
            cmdInsert.Parameters.AddWithValue("ExpiredDate", item.ExpiredDate);

            int insertId = (int)cmdInsert.ExecuteScalar();
            item.PaymentId = insertId;
            return insertId;
        }


            //TODO implement code for updating
            /*public bool UpdateReservation(Reservation reserv)
            {
                //return rowsAffected > 0;
            }*/
        }
}