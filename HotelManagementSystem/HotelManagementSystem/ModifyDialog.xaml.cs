﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;


namespace HotelManagementSystem
{
    /// <summary>
    /// Interaction logic for ModifyDialog.xaml
    /// </summary>
    public partial class ModifyDialog : MetroWindow
    {
        enum EItemSortOrder { ReservId, FirstName, LastName, CheckIn };
        EItemSortOrder ItemSortOrder = EItemSortOrder.ReservId;       // set default sort column

        List<Reservation> ReservationList = new List<Reservation>();
        
        public ModifyDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            
            ReservationList = Globals.Db.GetAllReservations();
            lvReservations.ItemsSource = ReservationList;
        }

        public void ReloadList()
        {
            try
            {
                List<Reservation> list = Globals.Db.GetAllReservations();
                // sort using Dynamic Linq
                switch (ItemSortOrder)
                {
                    case EItemSortOrder.ReservId:
                        list = list.OrderBy(t => t.ReservId).ToList<Reservation>();
                        break;
                    case EItemSortOrder.FirstName:
                        list = list.OrderBy(t => t.FirstName).ToList<Reservation>();
                        break;
                    case EItemSortOrder.LastName:
                        break;
                    case EItemSortOrder.CheckIn:
                        break;
                    default:
                        MessageBox.Show("Internal error: unknown sort column", "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                }

                //search part
                string searchStr = tbSearch.Text;
                if (searchStr != "")
                {
                    var query = from item in list where item.LastName.ToUpper().Contains(searchStr.ToUpper()) select item;
                    list = query.ToList();
                }

                ReservationList.Clear();
                ReservationList.AddRange(list.ToList());
                lvReservations.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "Hotel Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtModify_Click(object sender, RoutedEventArgs e)
        {
            AddUpdateDialog AddUpdate = new AddUpdateDialog(this);
            if (AddUpdate.ShowDialog() == true)
            {
                
            }
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            ReloadList();
        }

        private void LvItems_HeaderClick(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader HeaderClicked = e.OriginalSource as GridViewColumnHeader;

            if (HeaderClicked == null) { return; }

            if (HeaderClicked.Role != GridViewColumnHeaderRole.Padding)    
            {
                switch (HeaderClicked.Tag)
                {
                    case "ReservId":
                        ItemSortOrder = EItemSortOrder.ReservId;
                        break;
                    case "FirstName":
                        ItemSortOrder = EItemSortOrder.FirstName;
                        break;
                    case "LastName":
                        ItemSortOrder = EItemSortOrder.LastName;
                        break;
                    case "CheckIn":
                        ItemSortOrder = EItemSortOrder.CheckIn;
                        break;
                    default:
                        MessageBox.Show("Internal error: unknown sort column",
                            "Items Database", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                }
            }
            ReloadList();
        }

        private void BtDelete_Click(object sender, RoutedEventArgs e)
        {
            Reservation currReserv = lvReservations.SelectedItem as Reservation;
            MessageBoxResult result = MessageBox.Show(this, "Are you sure you want to permanently delete: reservation id " + currReserv.ReservId + "\n for the guest: " + currReserv.LastName + " " + currReserv.FirstName, "Confirm delete", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
            //
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeleteReservation(currReserv.ReservId);
                    ReloadList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                        "Hotels Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }

    }
}
