﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagementSystem
{
    public class RoomWithReserv
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public string Phone { get; set; }
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public EnRoomType RoomType { get; set; }
        public decimal PricePerNight { get; set; }
        public string MaxPerson { get; set; }
    }

    public enum EnRoomType { Single = 2, Double = 1 }
   
   
}
