using Microsoft.VisualStudio.TestTools.UnitTesting;
using UnitTestProject1;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LoadRoomsListbyDate()
        {
            DateTime currentDate = DateTime.Now;
            DateTime dateIn = (DateTime)dpDateIn.SelectedDate;
            DateTime dateOut = (DateTime)dpDateOut.SelectedDate;

            try
            {
                List<RoomWithReserv> list = Globals.Db.GetAllRoomsWithReserv();
                var reserv = from item in list where !(dateIn < item.CheckIn) && !(item.CheckIn < dateOut) select item;

                list = reserv.ToList();
                // Clear old ItemsList
                RoomListAvailablebyDate.Clear();
                // Add new list to ItemsList
                RoomListAvailablebyDate.AddRange(list);
                lvRoomAvailablebyDate.Items.Refresh();

                // ROOM HAD RESERVE
                List<RoomWithReserv> listU = Globals.Db.GetAllRoomsWithReserv();
                var reservOut = from row in listU where row.CheckOut < currentDate select row;
                listU = reservOut.ToList();
                RoomListAvailable.Clear();
                RoomListAvailable.AddRange(listU.ToList());

                List<RoomWithReserv> listA = Globals.Db.GetAllRoomsAvailable();
                var avail = from row in listA select row;
                listA = avail.ToList();
                RoomListAvailable.AddRange(listA.ToList());
                lvRoomAvailablebyDate.Items.Refresh();

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message, "Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
