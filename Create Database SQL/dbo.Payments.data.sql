SET IDENTITY_INSERT [dbo].[Payments] ON
INSERT INTO [dbo].[Payments] ([PaymentId], [CreditCardNumber], [ExpiredDate], [CVCNumber], [CardType]) VALUES (1, N'1234-1234-1234', N'2019-05-03', N'454', N'Visa        ')
INSERT INTO [dbo].[Payments] ([PaymentId], [CreditCardNumber], [ExpiredDate], [CVCNumber], [CardType]) VALUES (2, N'123-123-123-456', N'2019-12-10', N'454', N'Visa        ')
INSERT INTO [dbo].[Payments] ([PaymentId], [CreditCardNumber], [ExpiredDate], [CVCNumber], [CardType]) VALUES (3, N'1234-1234-1234', N'2020-06-12', N'454', N'Visa        ')
INSERT INTO [dbo].[Payments] ([PaymentId], [CreditCardNumber], [ExpiredDate], [CVCNumber], [CardType]) VALUES (5, N'123456789123', N'2019-05-13', N'123', N'MasterCard')
SET IDENTITY_INSERT [dbo].[Payments] OFF
