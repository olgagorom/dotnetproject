Use DotNetProject
--Use DotNetProba

create table Rooms
(
	RoomId int identity(1,1) NOT NULL,
	RoomNumber nvarchar(5) NOT NULL,
	RoomType nvarchar(6) NOT NULL, --enum
	PricePerNight decimal NOT NULL,
	MaxPerson nvarchar NOT NULL,
	RoomStatus nvarchar (10) NOT NULL  --enum

	constraint pk_RoomId primary key clustered (RoomId)
);
GO

alter table Rooms
		with nocheck
		add constraint ck_Rooms_RoomType check (RoomType IN ('single','double'));
	go

alter table Rooms
		with nocheck
		add constraint ck_Rooms_RoomStatus check (RoomStatus IN ('available','occupied', 'reserved'));
	go


create table Payments
(
	PaymentId int identity(1,1) NOT NULL,
	CreditCardNumber nvarchar(20) NOT NULL,
	ExpiredDate datetime NOT NULL,
	CVCNumber nvarchar(3) NOT NULL,
	CardType nvarchar(12) NOT NULL

	constraint pk_PaymentId primary key clustered (PaymentId)
);
GO

create table Users
(
	UserId int identity(1,1) NOT NULL,
	UserType nvarchar(10) NOT NULL, --enum
	UserName nvarchar(20) NOT NULL,
	UserPassword nvarchar(20) NOT NULL

	constraint pk_UserId primary key clustered (UserId)
);
GO

alter table Users
		with nocheck
		add constraint ck_UserType check (UserType IN ('admin','deskmanager', 'kitchen'));
	go


create table Reservations
(
	ReservId int identity(1,1) NOT NULL,
	CheckIn datetime NOT NULL,
	CheckOut datetime NOT NULL,
	RoomId int NOT NULL,
	Breakfast bit NOT NULL,
	Nights int NOT NULL,
	Comment nvarchar(200) NOT NULL,
	FirstName nvarchar(20) NOT NULL,
	LastName nvarchar(20) NOT NULL,
	Birthday datetime NOT NULL,
	Address nvarchar(60) NULL,
	PostalCode nvarchar(10) NULL,
	City nvarchar(20) NULL,
	Country nvarchar(20) NULL,
	Email nvarchar(60) NUll,
	Phone nvarchar(24) NULL,
	RegTime datetime NOT NULL,
	ReservType nvarchar (10) NOT NULL, -- enum
	PaymentId int NOT NULL
	
	constraint pk_ReservId primary key clustered (ReservId)
);

GO


-- CONSTRAINS 

alter table Reservations
		with nocheck
		add constraint ck_Reservations_ReservType check (ReservType IN ('phone','inperson', 'online'));
	go

alter table Reservations
		with nocheck
		add constraint fk_Reservations_Rooms foreign key (RoomId) references Rooms (RoomId);
	go

	alter table Reservations
		with nocheck
		add constraint fk_Reservations_Payments foreign key (PaymentId) references Payments (PaymentId);
	go



